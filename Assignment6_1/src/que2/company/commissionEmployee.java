package que2.company;

public class commissionEmployee extends Employee{

	double grossSales;
	double commissionRate;
	
	
	public commissionEmployee() {
		super();
	}
	public commissionEmployee(String first_name, String last_name, String ssn,double grossSales, double commissionRate) {
		super(first_name, last_name, ssn);
		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
	}
	@Override
	double calculateSalary() {
		return this.grossSales*this.commissionRate;
	}

}
