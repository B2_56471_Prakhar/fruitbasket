package que2.company;

public abstract class Employee {
	String first_name,last_name;
	String SSN;
	public Employee() {
		this("default","defualt","0000000");
	}
	public Employee(String first_name, String last_name, String ssn) {
		this.first_name = first_name;
		this.last_name = last_name;
		this.SSN = ssn;
	}
	
	abstract double calculateSalary();
	
}
