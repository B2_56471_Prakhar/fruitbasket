package que2.company;

public class hourlyEmployee extends Employee{

	double hourlyWage;
	int hoursWorked;
	
	public hourlyEmployee() {
		super();
	}
	
	public hourlyEmployee(String first_name, String last_name, String ssn,double hourlyWage, int hoursWorked) {
		super(first_name, last_name, ssn);
		this.hourlyWage = hourlyWage;
		this.hoursWorked = hoursWorked;
	}

	@Override
	double calculateSalary() {
		if(this.hoursWorked <=40)
			return this.hourlyWage*this.hoursWorked;
		else {
			return ( (40*this.hourlyWage)+(this.hoursWorked-40)*this.hourlyWage*1.5 );
		}
	}
	
	
		
}
