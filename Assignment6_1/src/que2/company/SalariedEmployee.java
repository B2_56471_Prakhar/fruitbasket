package que2.company;

public class SalariedEmployee extends Employee {
	double weeklySalary;

	public SalariedEmployee() {
		super();
	}

	public SalariedEmployee(String first_name, String last_name, String ssn,double salary) {
		super(first_name, last_name, ssn);
		this.weeklySalary =  salary;
	}

	double calculateSalary() {
		return this.weeklySalary;
	}
	
}
