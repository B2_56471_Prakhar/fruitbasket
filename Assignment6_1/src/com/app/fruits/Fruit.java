package com.app.fruits;

public class Fruit {
	private String color;
	private double weight;
	private String name;
	private boolean isFresh=true;
	public Fruit() {
		this("fruit",0.0,"default");
		
	}
	public Fruit(String name, double weight ,String color) {
		this.color = color;
		this.weight = weight;
		this.name = name;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isFresh() {
		return isFresh;
	}
	public void setFresh(boolean isFresh) {
		this.isFresh = isFresh;
	}
	public String taste() {
		return "no specific taste";
	}
	
	@Override
	public String toString() {
		return "Fruit [name=" + this.getName() + ", weight=" + this.getWeight() + ", color=" + this.getColor() + ", isFresh=" + this.isFresh() + "]";
	}
	
	
}
