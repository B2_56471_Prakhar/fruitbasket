package com.app.fruits;

import java.util.Scanner;

public class FruitBasket {

	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		
		int number;
		System.out.println("Enter number of fruits in the basket : ");
		number = sc.nextInt();
		
		Fruit[] basket = new Fruit[number];
		
		int counter = 0;
		
		while(true)
		{
			
			
			System.out.println("**********    MENU   **********");
			System.out.println("1. Add Mango");
			System.out.println("2. Add Orange");
			System.out.println("3. Add Apple");
			System.out.println("4. Display names of all fruits in the basket");
			System.out.println("5. Display name,color,weight of all fresh fruits");
			System.out.println("6. Display tastes of all stale(not fresh) fruits in the basket");
			System.out.println("7. Mark a fruit as stale");
			System.out.println("8. Mark all sour fruits stale");
			System.out.println("9. Exit");
			System.out.print("Enter choice : ");
			
			int choice;
			
			choice = sc.nextInt();
			
			switch(choice) {
					
			case 1:
				basket[counter++] = new Mango("Mango",3.5,"yellow");
				System.out.println("New mango added to the basket");
				break;
			case 2:
				basket[counter++] = new Orange("Orange",4.5,"orange");
				System.out.println("New orange added to the basket");

				break;
			case 3:
				basket[counter++] = new Apple("Apple",3.5,"red");
				System.out.println("New apple added to the basket");

				break;
			case 4:
				for(Fruit fruit : basket)
				{
					if(fruit == null)
						System.out.println("null fruit");
					else
						System.out.println(fruit.getName());
				}
				break;
			case 5:
				for(Fruit fruit : basket)
				{
					if(fruit == null)
						System.out.println("null fruit");
					else {
						fruit.toString();
						System.out.println("Taste : "+fruit.taste());
					}
				}
				break;
			case 6:
				for(Fruit fruit : basket)
					if(fruit.isFresh()==false)
						System.out.println(fruit.getName()+" "+fruit.taste());
				break;
			case 7:
				System.out.print("Enter which fruit number u have to mark as stale : ");
				int n =sc.nextInt();
				if(n <= basket.length)	
					if(basket[n-1].isFresh()==true)
						basket[n-1].setFresh(false);
					else
						System.out.println("the fruit is already stale(not fresh)");
				else
					System.out.println("invalid index");
				break;
			case 8:
				for(Fruit fruit:basket)
					if(fruit.taste()=="sour")
						fruit.setFresh(false);
				break;
			case 9:
				System.exit(0);
				break;
			}
		}

	}

}
